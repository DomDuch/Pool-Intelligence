<%--
  Created by IntelliJ IDEA.
  User: etien
  Date: 2017-05-07
  Time: 12:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pool-Intelligence</title>

    <style>
        tr:first-child{
            font-weight: bold;
            background-color: #C6C9C4;
        }
    </style>
</head>

<body>
<h2>List of Players</h2>
<table>
    <tr>
        <td>Fisrt Name</td>
        <td>Last Name</td>
    </tr>
    <c:forEach items="${players}" var="player">
        <tr>
            <td>${player.firstName}</td>
            <td>${player.lastName}</td>
            <td><a href="<c:url value='/edit-${player.ssn}-player'/>">${player.ssn}</a></td>
            <td><a href="<c:url value='/delete-${player.ssn}-player'/>">delete</a></td>
        </tr>
    </c:forEach>
</table>
<br/>
<a href="<c:url value='/new' />">Add New Player</a>
</body>
</html>
