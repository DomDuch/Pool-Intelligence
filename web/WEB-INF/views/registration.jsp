<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: etien
  Date: 2017-05-07
  Time: 1:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Player Registration Form</title>
    <style>
        .error{
            color:#ff0000;
        }
    </style>
</head>

<body>
<h2>Registration Form</h2>

<form:form method="POST" modelAttribute="player">
    <form:input type="hideen" path="id" id="id"/>
    <table>
        <tr>
            <td><label for="firstname">FirstName: </label></td>
            <td><form:input path="firstName" id="firstname"/></td>
            <td><form:errors path="firstName" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label for="lastname">FirstName: </label></td>
            <td><form:input path="lastName" id="lastname"/></td>
            <td><form:errors path="lastName" cssClass="error"/></td>
        </tr>

        <tr>
            <td colspan="2">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>
Go back to <a href="<c:url value='/list' />">List of All players</a>
</body>
</html>
