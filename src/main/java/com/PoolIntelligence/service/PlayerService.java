package com.PoolIntelligence.service;

import com.PoolIntelligence.dao.PlayerRepository;
import com.PoolIntelligence.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by etien on 2017-05-07.
 */

@Service("playerService")
@Transactional
public class PlayerService {

    @Autowired
    private PlayerRepository playerDao;

    public Player findById(Long id){
        return playerDao.findById(id);
    }

    public Iterable<Player> findAllPlayers() {
        return playerDao.findAll();
    }

    public void savePlayer(Player player){
        playerDao.save(player);
    }

}
