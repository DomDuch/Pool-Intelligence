package com.PoolIntelligence.dao.custom;

import com.PoolIntelligence.model.Player;

/**
 * Created by dominic on 5/7/17.
 */

public interface PlayerRepositoryCustom {
    public void someCustomMethod(Player player);
}
