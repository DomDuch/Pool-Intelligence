package com.PoolIntelligence.dao;

import com.PoolIntelligence.dao.custom.PlayerRepositoryCustom;
import com.PoolIntelligence.model.Player;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends CrudRepository<Player, Long>, PlayerRepositoryCustom {
    Player findById(Long id);
    List<Player> findByFirstName(String firstName);
}